#let's start customizing the desktop

%post
echo "`date -Is` Starting ci-desktop.ks post section" >> /var/log/ci-spin.log

cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.input-sources.gschema.override << FOE
[org.gnome.desktop.input-sources]
show-all-sources=true
xkb-options=['grp:alt_shift_toggle', 'terminate:ctrl_alt_bksp', 'lv3:switch', 'numpad:microsoft']
per-window=false
current=uint32 0
mru-sources=[('xkb', 'ci+civ'), ('xkb', 'ci+civ_qw'), ('xkb', 'ci')]
sources=[('xkb', 'ci+civ'), ('xkb', 'ci+civ_qw'), ('xkb', 'ci')]
FOE
# sources=[('xkb', 'ci+civ'), ('xkb', 'ci+civ_qw'), ('xkb', 'ci'), ('xkb', 'ca+eng')]

cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.background.gschema.override << FOE
[org.gnome.desktop.background]
picture-uri='file:////usr/share/gnome-control-center/pixmaps/noise-texture-light.png'
color-shading-type='solid'
primary-color='#425265'
picture-options='wallpaper'
secondary-color='#425265'
FOE

cat >> /usr/share/glib-2.0/schemas/org.gnome.shell.gschema.override << FOE
[org.gnome.shell]
app-picker-view=uint32 0
enabled-extensions=['places-menu@gnome-shell-extensions.gcampax.github.com', 'apps-menu@gnome-shell-extensions.gcampax.github.com', 'dash-to-dock@micxgx.gmail.com', 'user-theme@gnome-shell-extensions.gcampax.github.com']
favorite-apps=['org.gnome.Terminal.desktop', 'org.gnome.Nautilus.desktop', 'anki.desktop', 'libreoffice-writer.desktop', 'omegat.desktop', 'poedit.desktop', 'anaconda.desktop', 'google-chrome.desktop']
had-bluetooth-devices-setup=true
FOE

cat >> /usr/share/glib-2.0/schemas/org.gnome.shell.extensions.dash-to-dock.gschema.override << FOE
[org.gnome.shell.extensions.dash-to-dock]
apply-custom-theme=true
dock-position='BOTTOM'
custom-theme-shrink=true
background-opacity=0.88
dash-max-icon-size=32
opaque-background=true
shortcut=['<Super>q']
FOE

cat >> /usr/share/glib-2.0/schemas/org.gnome.shell.extensions.user-theme.gschema.override << FOE
[org.gnome.shell.extensions.user-theme]
name='EvoPop'
FOE

cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.interface.gschema.override << FOE
[org.gnome.desktop.interface]
clock-show-date=true
gtk-im-module='gtk-im-context-simple'
icon-theme='Paper'
gtk-theme='EvoPop'
clock-format='24h'
FOE
echo "`date -Is` Finished processing ci-desktop.ks" >> /var/log/ci-spin.log

#mkdir -p /etc/skel/.config/autostart
#cat >> /etc/skel/.config/autostart/getmestarted.desktop <<FOE
#[autorun]Name=Autostart
#Comment="Run on login"
#Exec=~/.local/bin/autorun.sh
#Terminal=false
#Type=Application
#FOE

#fix this later if need be
mkdir -p /etc/skel/.local/bin
cat >> /etc/skel/.local/bin/autorun.sh <<FOE
#!/bin/bash
#yes | pip -q install --user translate-tools
FOE
chmod +x /etc/skel/.local/bin/autorun.sh

cat >> /usr/local/share/applications/omegat.desktop <<FOE
[Desktop Entry]
Name=OmegaT
Exec=/usr/local/OmegaT_4.1.4_Beta_Linux_64/OmegaT
Type=Application
StartupNotify=true
Icon=
Path=/usr/local/OmegaT_4.1.4_Beta_Linux_64
StartupWMClass=
Categories=Application;Office
FOE

%end
