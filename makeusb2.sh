#!/bin/bash
#sudo livecd-iso-to-disk --efi --format --msdos  ./iso/Fedora-Workstation.iso /dev/sdb
if [ ! -h /dev/disk/by-label/CI-SPIN ]; then
	pacat --volume 65000  /usr/share/sounds/gnome/default/alerts/bark.ogg
	exit
fi

umount -l /dev/disk/by-label/CI-SPIN
sudo livecd-iso-to-disk --efi \
	--home-size-mb 3000 \
	--unencrypted-home \
	--format --reset-mbr /mnt/windows/Users/Boyd/Documents/CI-SPIN.iso /dev/disk/by-label/CI-SPIN

pacat --volume 65000  /usr/share/sounds/gnome/default/alerts/bark.ogg
