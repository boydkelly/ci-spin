%packages

# Exclude unwanted groups that fedora-live-base.ks pulls in
-@dial-up
-@input-methods
-@standard
@base-x
@core
@gnome-desktop
-@firefox
-@fonts
-@guest-desktop-agents
@libreoffice
-@networkmanager-submodules
@workstation-product
-abrt-*
-autofs
-gfs2-utils
-reiserfs-utils

#Fonts
abattis-cantarell-fonts
liberation-mono-fonts                                            
liberation-sans-fonts                                            
liberation-serif-fonts                                           
terminus-fonts-console                                           
powerline-fonts

#stuff from workstation ks
#for production image, enable chromium,printing,hardware support,multimedia,networkmanager-submodules ---------------  disable desktop guests

-@hardware-support
-@multimedia
@printing

NetworkManager-wifi

#troubleshooting (comment out for prod)
dconf-editor
gnome-tweaks

#extra gnome custom stuff
xdg-user-dirs   #needed to create folders in home directory
evopop-gtk-theme
paper-icon-theme
f28-backgrounds-base
#f28-backgrounds-extras-base
#f28-backgrounds-gnome
#f28-backgrounds-extras-gnome
gnome-shell-extension-dash-to-dock
gnome-shell-extension-user-theme
gnome-shell-extension-apps-menu
gnome-shell-extension-places-menu

fedora-workstation-repositories
rpmfusion-free-release
#rpmfusion-free-appstream-data
#libass
#mpv

translate-toolkit
tw
virtaal
anki
sox
poedit
gtranslator
libreoffice-langpack-fr
#python2-pip
ssmtp
#gparted

#bitext2tmx-1.0M0-080229.noarch

#Add some stuff
#gnome-classic-session

#Add a browser
#firefox
#google-chrome-ustable
#chromium
#midori
epiphany

#login authselect error
libffi
# gdm x error
libxshmfence
libSM
libXtst

#-sane-backends in fedora-live-min error
-simple-scan
-libsane*
-sane-backends-*
%end

