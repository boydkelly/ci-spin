#!/bin/bash
export LNAME=${PWD##*/}
export NAME=`echo $LNAME | tr '[:lower:]' '[:upper:]'`
export LOCATION="${LNAME}/isos"
export VER="28"

ksflatten --config ${LNAME}.ks -o flat-${LNAME}.ks --version F${VER}
exit
livemedia-creator --ks flat-${LNAME}.ks --no-virt --resultdir /var/lmc --project ${NAME} --make-iso --volid ${NAME}-${VER} --iso-only --iso-name ${LNAME}.iso --releasever ${VER} --title ${NAME} --macboot

if [ $retval -eq 0 ]; then
	if [ $HOSTNAME = "y2" ]; then
		echo $retval
		echo "We are moving iso!"
		mv -f ${NAME}.iso ${LOCATION}/${LNAME}.iso
		if [ $retval -eq 0 ];then
			rm -f ${NAME}.iso
		fi
		sudo rm -fr /var/tmp/img*
	fi
	echo "`hostname`" | mail -s "Success!  Created ${LNAME}.iso at `date`" bkelly@coastsystems.net
fi

