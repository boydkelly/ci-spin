#!/bin/bash
#sudo livecd-iso-to-disk --efi --format --msdos  ./iso/Fedora-Workstation.iso /dev/sdb
sudo livecd-iso-to-disk --efi \
	--format --msdos --reset-mbr \
	--home-size-mb 3000 \
	--unencrypted-home /mnt/windows/Users/Boyd/Documents/Workstation-CI.iso /dev/sdc

pacat --volume 65000  /usr/share/sounds/gnome/default/alerts/bark.ogg
