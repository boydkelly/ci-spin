%post --nochroot
#really shouldnt have to do this
#cp -prv /etc/skel/{.bashrc,.bash_profile} $INSTALL_ROOT/etc/skel/
cp -pvr /etc/ssmtp/ssmtp.conf $INSTALL_ROOT/etc/ssmtp/
tar -C $INSTALL_ROOT/usr/local/ -xzf omegat-4.1.5_01.tar.gz 
#tar -C $INSTALL_ROOT/usr/share/gnome-shell/extensions -xf dash2dock.tar  

#mkdir -vp $INSTALL_ROOT/home/liveuser/.local/share
#tar zxf Anki2.tgz -C $INSTALL_ROOT/home/liveuser/.local/share/
#tar -C $INSTALL_ROOT/home/liveuser/.local/share/ -xzf Anki2.tgz
mkdir -vp $INSTALL_ROOT/etc/skel/.local/share
tar -C $INSTALL_ROOT/etc/skel/.local/share/ -xzf Anki2.tgz
git submodule update
pushd dash-to-dock
git fetch --all --tags --prune
git checkout extensions.gnome.org-v64
DESTDIR=$INSTALL_ROOT/ make install
pushd
echo $(date -I)-ci-spin  > /home/bkelly/livecd/install.log

%end
